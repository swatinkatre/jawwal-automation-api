<soapenv:Envelope xmlns:con="http://ericsson.com/services/ws_CIL_7/contractread" xmlns:ses="http://ericsson.com/services/ws_CIL_7/sessionchange" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
         <wsse:UsernameToken wsu:Id="UsernameToken-051F01F6EA2C7A70F615392493247174">
            <wsse:Username>ADMX</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">ADMX</wsse:Password>
            <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">+IzGKQ5EKW/uNN7981VjKA==</wsse:Nonce>
            <wsu:Created>2018-10-11T09:15:24.717Z</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <con:contractReadRequest>
         <!--You may enter the following 2 items in any order-->
         <!--Optional:-->
         <con:inputAttributes>
            <!--You may enter the following 3 items in any order-->
            <!--Optional:-->
            <con:coId/>
            <!--Optional:-->
            <con:coIdPub>ContractNumberValue</con:coIdPub>
            <!--Optional:-->
            <con:syncWithDb/>
         </con:inputAttributes>
         <!--Optional:-->
         <con:sessionChangeRequest>
            <!--Optional:-->
            <ses:values>
               <!--1 or more repetitions:-->
               <ses:item>
                  <!--You may enter the following 2 items in any order-->
                  <ses:key>BU_ID</ses:key>
                  <ses:value>2</ses:value>
               </ses:item>
            </ses:values>
         </con:sessionChangeRequest>
      </con:contractReadRequest>
   </soapenv:Body>
</soapenv:Envelope>
