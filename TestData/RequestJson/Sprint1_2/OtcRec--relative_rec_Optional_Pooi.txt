{
              "scId": "scIDValue",
              "biType": "ProductOfferingOrder",
              "action": "Add",
              "relatedParties": [
                             {
                                           "role": "Customer",
                                           "reference": "otcRec--relative_rec_cusid"
                             }
              ],
              "relatedEntities": [
                             {
                                           "type": "Contract",
                                           "reference": "otcRec--relative_rec_contractid"
                             }
              ],
              "relatedItems": [
                             {
                                           "role": "ReliesOn",
                                           "reference": "SCPoid"
                             }
              ],
              "attrs": [
                             {
                                           "name": "Relative_Flag",
                                           "value": "R"
                             }
              ],
              "product": {
                             "productCharacteristics": [
                                           {
                                                          "name": "POP_OTC_CODE",
                                                          "value": "otc_value_pop"
                                           },
                                           {
                                                          "name": "POP_REC_CODE",
                                                          "value": "rec_value_pop"
                                           }
                             ],
                             "productPrice": [
            {
                 
                "popId": "otc_value_pop",
                "characteristics": [
                    {
                        "name": "ONETIMEAMT_PERS",
                        "value": "1000"
                    }
                ]
            },
            {
                "popId": "rec_value_pop",
                "characteristics": [
                    {
                        "name": "RECAMNT_BC_PERS",
                        "value": "override_recvalue"
                    },
                    {
                        "name": "RECAMNT_PERS",
                        "value": "override_recamtvalue"
                    }
                ]
                
            }
            
       ]
              },
              "resources":[{
            "resourceCharacteristics": [
                {
                    "name": "subMarket",
                    "value": "ISDNi"
                },
                {
                    "name": "resourceNumber",
                    "value": "otcRec--relative_rec_resourceid"
                }
            ],
            "resourceSpecification": "LRS_NAI"
        },
         {
            "resourceCharacteristics": [
                {
                    "name": "subMarket",
                    "value": "ISDNi"
                },
                {
                    "name": "resourceNumber",
                    "value": "otcRec--relative_rec_resourcenum"
                }
            ],
            "resourceSpecification": "LRS_PRIVATE_TINCO"
        }],
              "productOffering": {
                             "id": "optional_po"
              }
}
