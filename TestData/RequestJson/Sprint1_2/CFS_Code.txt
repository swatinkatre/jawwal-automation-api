<soapenv:Envelope xmlns:prod="http://ericsson.com/services/ws_CIL_6/productcfsread" xmlns:ses="http://ericsson.com/services/ws_CIL_6/sessionchange" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
         <wsse:UsernameToken wsu:Id="UsernameToken-D22BC4309528EFE62715446959253861">
            <wsse:Username>ADMX</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">ADMX</wsse:Password>
            <wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">GopgMRyJvPJMTDw7jaYWyA==</wsse:Nonce>
            <wsu:Created>2018-12-13T10:12:05.383Z</wsu:Created>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <prod:productCfsReadRequest>
         <!--You may enter the following 2 items in any order-->
         <!--Optional:-->
         <prod:inputAttributes>
            <!--Optional:-->
            <prod:productCfsReadInputDTO>
               <!--You may enter the following 2 items in any order-->
               <prod:contractIdPub>ContractIdValue</prod:contractIdPub>
               <!--Optional:-->
               <prod:productId></prod:productId>
            </prod:productCfsReadInputDTO>
         </prod:inputAttributes>
         <!--Optional:-->
         <prod:sessionChangeRequest>
            <!--Optional:-->
            <ses:values>
               <!--1 or more repetitions:-->
               <ses:item>
                  <!--You may enter the following 2 items in any order-->
                  <ses:key>BU_ID</ses:key>
                  <ses:value>2</ses:value>
               </ses:item>
            </ses:values>
         </prod:sessionChangeRequest>
      </prod:productCfsReadRequest>
   </soapenv:Body>
</soapenv:Envelope>